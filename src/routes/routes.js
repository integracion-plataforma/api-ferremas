
const config = require("../config/global");
const globalPathPrefix = config.get("globalPathPrefix");
const healthcheckRoute = require('../components/healthcheck/healthcheck.routes');
const createUser = require('../components/createUser/create-users.routes');
const cryptoKey = require('../components/encryption/encryption.routes');
const autenticator = require('../components/autentication/auth-user.routes');
const getProduct = require('../components/getProduct/getProducts.routes');
const createProduct = require('../components/createProduct/createProduct.routes');
const updateProduct = require('../components/updateProduct/updateProduct.routes');
const deleteProduct = require('../components/deleteProduct/deleteProduct.routes');
const createTransaction = require('../components/createTransaction/createTransaction.routes');
const confirmTransaction = require('../components/confirmTransaction/confirmTransaction.routes');
const updateStock = require('../components/updateStock/updateStock.routes');

function routes(app) {
  healthcheckRoute(app, globalPathPrefix);
  autenticator(app, globalPathPrefix);
  createUser(app, globalPathPrefix);
  cryptoKey(app, globalPathPrefix);
  getProduct(app, globalPathPrefix);
  createProduct(app, globalPathPrefix);
  updateProduct(app, globalPathPrefix);
  deleteProduct(app, globalPathPrefix);
  createTransaction(app, globalPathPrefix);
  confirmTransaction(app, globalPathPrefix);
  updateStock(app, globalPathPrefix);
}

module.exports = {
  routes
};
