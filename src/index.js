"use strict";

const express = require("express");
const helmet = require("helmet");
const cors = require('cors');

const { routes } = require("./routes/routes");
const config = require("./config/global");
const mysqlUtil = require("./util/mysqlPool.util.js");

const app = express();

async function serverStart() {
  try {
    app.use(express.json());

    app.use(helmet());
    app.use(cors({ origin: '*' }));

    routes(app);

    await connectDB();

    let port = config.port;
    app.listen(port, () => {
      console.log(
        `Servidor ejecutandose en el puerto: ${port}, con ruta base ${config.globalPathPrefix}`
      );
    });
  } catch (error) {
    console.log(error);
    await cleanup();
  }
}

async function connectDB() {
  await mysqlUtil.connectReadPool();
  await mysqlUtil.connectWritePool();
}

async function disconnectDB() {
  await mysqlUtil.getReadPool().destroy();
  await mysqlUtil.getWritePool().destroy();
}

async function cleanup() {
  try {
    console.log("Desconectando...");
    await disconnectDB();
    process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
}

serverStart();
