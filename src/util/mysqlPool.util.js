"use strict";
const config = require("../config/global");
const knex = require("knex");

let readOnlyPool;
let readWritePool;

async function connectReadPool() {
  if (readOnlyPool) return readOnlyPool;

  // Knex Mysql Read Only Pool
  // Se configura el máximo y mínimo del pool de conexiones
  const options = {};
  Object.assign(options, config.mysql.options);
  options.pool.max = config.mysql.pools.read.max;
  options.pool.min = config.mysql.pools.read.min;

  readOnlyPool = await knex(options);
  return readOnlyPool;
}

async function connectWritePool() {
  if (readWritePool) return readWritePool;

  // Knex Mysql Read Write Pool
  // Se configura el máximo y mínimo del pool de conexiones
  const options = {};
  Object.assign(options, config.mysql.options);
  options.pool.max = config.mysql.pools.write.max;
  options.pool.min = config.mysql.pools.write.min;

  readWritePool = await knex(options);
  return readWritePool;
}

function getReadPool() {
  return readOnlyPool;
}

function getWritePool() {
  return readWritePool;
}

module.exports = {
  connectReadPool,
  connectWritePool,
  getReadPool,
  getWritePool
};
