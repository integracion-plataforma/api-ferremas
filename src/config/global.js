"use strict";

const parseConfig = require("../util/parse-config.util");

const config = parseConfig({
  globalPathPrefix: {
    doc: "path base del microservicio",
    default: "/api/v1/ferremas-api"
  },
  port: {
    doc: "puerto de aplicación",
    env: "PORT",
    required: true
  },
  mysql: {
    options: {
      client: {
        doc: "tipo de cliente a usar por knex",
        default: "mysql"
      },
      connection: {
        host: {
          doc: "ip servidor Mysql",
          env: "MYSQL_HOST",
          required: true
        },
        port: {
          doc: "puerto servicio mysql",
          env: "MYSQL_PORT",
          required: true
        },
        user: {
          doc: "usuario Mysql",
          env: "MYSQL_USER",
          required: false
        },
        password: {
          doc: "contraseña Mysql",
          env: "MYSQL_PASSWORD",
          required: false
        },
        database: {
          doc: "nombre base Mysql",
          env: "MYSQL_DATABASE",
          required: true
        }
      },
      pool: {
        doc: "parámetros pool de conexiones",
        default: { min: 0, max: 10 }
      },
      acquireConnectionTimeout: {
        doc: "timeout para intentos de conexión Mysql",
        default: 15000
      }
    },
    pools: {
      read: {
        max: {
          doc: "mysql read pool max connections",
          env: "MAX_POOL_DB_READ",
          default: 5
        },
        min: {
          doc: "mysql read pool min connections",
          env: "MIN_POOL_DB_READ",
          default: 0
        }
      },
      write: {
        max: {
          doc: "mysql write pool max connections",
          env: "MAX_POOL_DB_WRITE",
          default: 10
        },
        min: {
          doc: "mysql write pool min connections",
          env: "MIN_POOL_DB_WRITE",
          default: 0
        }
      }
    }
  },
});

module.exports = config;
