const confirmTransactionController = require('./confirmTransaction.controller');


function route(app, globalPathPrefix) {
  app.put(
    `${globalPathPrefix}/confirmTransaction`,
    confirmTransactionController.confirm
  );
}

module.exports = route;
