const { Options, IntegrationCommerceCodes, IntegrationApiKeys, Environment } = require('transbank-sdk');
const WebpayPlus = require('transbank-sdk').WebpayPlus;


async function confirm(req, res) {
  try {
    const { token } = req.body;

    WebpayPlus.configureForTesting();
    const tx = new WebpayPlus.Transaction(new Options(IntegrationCommerceCodes.WEBPAY_PLUS, IntegrationApiKeys.WEBPAY, Environment.Integration));
    const response = await tx.commit(token);

    return res.status(200).json({
      msg: 'Transaccion confirmada',
      result: { 
        confirm: response.response_code === 0 
      }
    });
  } catch (error) {
    return res.status(200).json({
      msg: 'Transaccion confirmada',
      result: {
        confirm: false
      }
    });
  }
}

module.exports = {
  confirm
};
