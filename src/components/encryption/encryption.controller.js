const crypto = require("crypto");

const mysqlUtil = require("../../util/mysqlPool.util");

async function GetPairKey(req, res) { 

  const { rut } = req.params;

  try {
    const { publicKey, privateKey } = genKeyPair();

    const dataKey = {
      rut: rut,
      publicKey: publicKey,
      privateKey: privateKey
    };

    const poolWrite = mysqlUtil.getWritePool();
    await poolWrite("usuarios").insert(dataKey).onConflict('rut').merge({
      publicKey: dataKey.publicKey,
      privateKey: dataKey.privateKey
    });

    return res.status(200).json({
      code: "000",
      msg: 'Claves generadas',
      publicKey
    });

  } catch (error) {
    console.log('Error generando las claves: ', error.message);
    return res.status(500).json({
      code: "500",
      msg: error.message
    });
  }
}

function genKeyPair() {

  const keyPair = crypto.generateKeyPairSync('rsa', {
    modulusLength: 4096,
    publicKeyEncoding: {
      type: 'pkcs1',
      format: 'pem'
    },
    privateKeyEncoding: {
      type: 'pkcs1',
      format: 'pem'
    }
  });

  return {
    publicKey: keyPair.publicKey,
    privateKey: keyPair.privateKey
  };

}


module.exports = { GetPairKey };