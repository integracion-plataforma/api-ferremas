const encryptController = require('./encryption.controller');

function route(app, globalPathPrefix) {
  app.get(
    `${globalPathPrefix}/rut/:rut`,
    encryptController.GetPairKey
  );
}

module.exports = route;