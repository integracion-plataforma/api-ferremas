const { Options, IntegrationCommerceCodes, IntegrationApiKeys, Environment } = require('transbank-sdk');
const WebpayPlus = require('transbank-sdk').WebpayPlus;


async function create(req, res) {
  try {
    const { amount } = req.body;
    const buy_order = `integracion-${Math.floor(Math.random() * 10000) + 1}`;
    const session_id = `${Math.floor(Math.random() * 10000) + 1}`;
    const return_url = 'http://localhost:4200/confirm-transaction';

    WebpayPlus.configureForTesting();
    const tx = new WebpayPlus.Transaction(new Options(IntegrationCommerceCodes.WEBPAY_PLUS, IntegrationApiKeys.WEBPAY, Environment.Integration));
    const resp = await tx.create(buy_order, session_id, amount, return_url);

    return res.status(200).json({
      msg: 'Transaccion creada',
      result: resp
    });
  } catch (error) {
    return res.status(500).json({
      code: 500,
      estado: false,
      msg: error.message
    });
  }
}

module.exports = {
  create
};
