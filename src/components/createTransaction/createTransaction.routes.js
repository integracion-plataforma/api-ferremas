const createTransactionController = require('./createTransaction.controller');


function route(app, globalPathPrefix) {
  app.post(
    `${globalPathPrefix}/createTransaction`,
    createTransactionController.create
  );
}

module.exports = route;
