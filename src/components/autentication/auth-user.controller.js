const fs = require('fs');
const crypto = require('crypto');
const bcrypt = require('bcrypt');

const mysqlUtil = require("../../util/mysqlPool.util");

async function autentication(req, res) {

  const { rut, password } = req.body;

  try {
    const userDataPool = await mysqlUtil.getWritePool();
    const userData = await userDataPool('usuarios').where({ rut }).first();
    if (!userData) {
        return res.status(200).json({
          code: "001",
          msg: 'Credenciales incorrectas.'
        });
      }
      
    const decryptPassword = decryptedPassword(password, userData.privateKey);

    const validPassword = await bcrypt.compareSync(decryptPassword, userData.password);

    if ( !validPassword) {
      return res.status(200).json({
        code: "001",
        msg: 'Credenciales incorrectas.'
      });
    }

    return res.status(200).json({
      code: "000",
      msg: 'Credenciales correctas'
    });

  } catch (error) {
    console.log(error.message);
    return res.status(500).json({
      code: 500,
      estado: false,
      msg: error.message
    });
  }
}

function decryptedPassword(password, privateKey){

  try {

    const decryptData = crypto.privateDecrypt({
      key: privateKey,
      padding: crypto.constants.RSA_PKCS1_PADDING

    }, 
    Buffer.from(password, 'base64'));
    
    return decryptData.toString();

  } catch (error) {
    console.log('Error de desencriptación', error.message);
    return null;
  }
}

module.exports = {
  autentication
};
