const authController = require('./auth-user.controller');
const loginValidation = require('./auth-user.validationSchema')
const expressValidatorMiddleware = require('../../middlewares/express-validator.middleware');

function route(app, globalPathPrefix) {
  app.post(
    `${globalPathPrefix}/login/user`,
    loginValidation,
    expressValidatorMiddleware,
    authController.autentication
  );
}

module.exports = route;
