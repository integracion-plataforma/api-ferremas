const { body } = require("express-validator");

const loginValidation = [
  body("rut")
    .exists({ checkFalsy: true }).withMessage("El Rut es obligatorio").bail()
    .isLength({ min: 7 }).withMessage("Rut de largo mínimo 7 caracteres"),
  body("password")
    .exists({ checkFalsy: true }).withMessage("La contraseña es obligatoria").bail()
    .isLength({ min: 4 }).withMessage("Contrase de largo mínimo 4 caracteres")
];

module.exports = loginValidation;
