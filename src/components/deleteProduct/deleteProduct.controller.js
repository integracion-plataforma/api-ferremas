const mysqlUtil = require("../../util/mysqlPool.util");

async function deleteProduct(req, res) {

  try {
    const { id } = req.params;

    const poolWrite = mysqlUtil.getWritePool();

    await poolWrite('precios_productos').where({ id_producto: id }).delete();
    await poolWrite('productos').where({ id_producto: id }).delete();
    
    return res.status(200).json({
      msg: 'Producto eliminado'
    });

  } catch (error) {
    return res.status(500).json({
      code: 500,
      estado: false,
      msg: error.message
    });
  }
}

module.exports = {
    deleteProduct
};
