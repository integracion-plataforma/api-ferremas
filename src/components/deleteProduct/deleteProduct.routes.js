const deleteProductController = require('./deleteProduct.controller');


function route(app, globalPathPrefix) {
  app.delete(
    `${globalPathPrefix}/deleteProduct/:id`,
    deleteProductController.deleteProduct
  );
}

module.exports = route;
