const mysqlUtil = require("../../util/mysqlPool.util");

async function getProducts(req, res) {

  try {

    const poolRead = mysqlUtil.getReadPool();
    const productos = await poolRead.raw(
      `
      SELECT
      p.codigo_producto AS 'codigo',
      p.nombre AS 'nombre',
      p.marca AS 'marca',
      p.stock AS 'stock',
      ROUND(pp.precio / 100) * 100 AS 'precio',
      DATE_FORMAT(pp.fecha, '%d-%m-%Y') AS 'fecha'
      FROM
          productos p
      LEFT JOIN (
          SELECT
              id_producto,
              precio,
              fecha
          FROM
              precios_productos
          WHERE
              (id_producto, fecha) IN (
                  SELECT
                      id_producto,
                      MAX(fecha)
                  FROM
                      precios_productos
                  GROUP BY
                      id_producto
              )
      ) pp ON p.id_producto = pp.id_producto
      ORDER BY
      p.id_producto ASC;
      `
    );

    return res.status(200).json({
      msg: 'Productos obtenidos',
      productos: productos ? productos[0] : []
    });

  } catch (error) {
    return res.status(500).json({
      code: 500,
      estado: false,
      msg: error.message
    });
  }
}

module.exports = {
    getProducts
};
