const getProductController = require('./getProducts.controller');

function route(app, globalPathPrefix) {
  app.get(
    `${globalPathPrefix}/productos`,
    getProductController.getProducts
  );
}

module.exports = route;