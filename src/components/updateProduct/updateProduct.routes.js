const updateProductController = require('./updateProduct.controller');


function route(app, globalPathPrefix) {
  app.put(
    `${globalPathPrefix}/updateProduct/:id`,
    updateProductController.updateProduct
  );
}

module.exports = route;
