const mysqlUtil = require("../../util/mysqlPool.util");

async function updateProduct(req, res) {

  try {

    const { codigo_producto, nombre, marca, stock, precio } = req.body;
    const { id } = req.params;

    const productData = {
      codigo_producto,
      nombre,
      marca,
      stock
    };

    const poolWrite = mysqlUtil.getWritePool();
    await poolWrite('productos').where({ id_producto: id }).update(productData);
    
    if (precio) {
      await poolWrite('precios_productos').insert({
        id_producto: id,
        fecha: new Date(),
        precio: precio
      });
    } 

    return res.status(200).json({
      msg: 'Producto actualizado',
      productData: {
        ...productData,
        precio
      }
    });

  } catch (error) {
    return res.status(500).json({
      code: 500,
      estado: false,
      msg: error.message
    });
  }
}

module.exports = {
    updateProduct
};
