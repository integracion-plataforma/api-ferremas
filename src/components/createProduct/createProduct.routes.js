const createProductController = require('./createProduct.controller');


function route(app, globalPathPrefix) {
  app.post(
    `${globalPathPrefix}/createProduct`,
    createProductController.createProduct
  );
}

module.exports = route;
