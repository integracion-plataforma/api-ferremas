
const mysqlUtil = require("../../util/mysqlPool.util");

async function createProduct(req, res) {

  try {

    const { codigo_producto, nombre, marca, stock, precio } = req.body;
    const productData = {
      codigo_producto,
      nombre,
      marca,
      stock
    };

    const poolWrite = mysqlUtil.getWritePool();
    const insertProduct = await poolWrite("productos").insert(productData).returning('id_producto');
    const productId = insertProduct[0];
    
    if (!precio) {
      return res.status(400).json({
        message: 'Precio no creado, solo el producto fue creado'
      });
    } else {
      await poolWrite('precios_productos').insert({
        id_producto: productId,
        fecha: new Date(), // Fecha actual
        precio: precio
    });
    }

    return res.status(200).json({
      msg: 'Producto creado',
      productData: {
        ...productData,
        precio
      }
    });

  } catch (error) {
    return res.status(500).json({
      code: 500,
      estado: false,
      msg: error.message
    });
  }
}

module.exports = {
    createProduct
};
