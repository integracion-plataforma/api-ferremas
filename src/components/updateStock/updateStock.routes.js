const updateStockController = require('./updateStock.controller');


function route(app, globalPathPrefix) {
  app.put(
    `${globalPathPrefix}/updateStock`,
    updateStockController.updateStock
  );
}

module.exports = route;
