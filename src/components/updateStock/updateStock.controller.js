const mysqlUtil = require("../../util/mysqlPool.util");

async function updateStock(req, res) {
    try {
        const productos = req.body.products;
        const poolWrite = mysqlUtil.getWritePool();
        const poolRead = mysqlUtil.getReadPool();
    
        // Inicia la transacción
        for (const producto of productos) {
          const { item, quantity } = producto;

          const productStock = await poolRead.raw(`SELECT stock FROM productos WHERE codigo_producto = '${item.codigo}'`);
          const stockP = productStock[0][0].stock;

          await poolWrite('productos').where({ codigo_producto: item.codigo }).update({ stock: stockP - quantity });
        }
        return res.status(200).json({ 
          message: "Compra finalizada y stock actualizado correctamente." 
        });
      } catch (error) {
        return res.status(500).json({
          code: 500,
          estado: false,
          msg: error.message
        });
    }
}

module.exports = {
    updateStock
};
