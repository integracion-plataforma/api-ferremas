"use strict"

const mysqlUtil = require('../../util/mysqlPool.util');

async function healthcheck(req, res) {
  try {
    const poolRead = mysqlUtil.getReadPool();
    const response = await poolRead.raw("SELECT 1");

    if (response) {
      return res.status(200).send({ status: "OK" });  
    } else {
      return res.status(500).send({ status: "ERROR DB" });
    }
  } catch (error) {
    return res.status(500).send({ status: "ERROR DB" });
  }
}

module.exports = healthcheck;
