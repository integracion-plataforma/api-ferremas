const createUserController = require('./create-users.controller');
const createUserValidationSchema = require('./create-users.validationSchema');
const expressValidatorMiddleware = require('../../middlewares/express-validator.middleware');

function route(app, globalPathPrefix) {
  app.post(
    `${globalPathPrefix}/createUser`,
    createUserValidationSchema,
    expressValidatorMiddleware,
    createUserController.createUser
  );
}

module.exports = route;
