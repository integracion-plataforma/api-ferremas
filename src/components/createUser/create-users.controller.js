const bcrypt = require('bcrypt');
const mysqlUtil = require("../../util/mysqlPool.util");

async function createUser(req, res) {

  try {

    const salt = bcrypt.genSaltSync();

    hashedPassword = bcrypt.hashSync(req.body.password, salt);

    const bodyUser = {
        rut: req.body.rut,
        dv: req.body.dv,
        password: hashedPassword,
        name: req.body.name,
    };

    const poolWrite = mysqlUtil.getWritePool();
    await poolWrite("usuarios").insert(bodyUser);

    return res.status(200).json({
      msg: 'Usuario creado',
      bodyUser
    });

  } catch (error) {
    return res.status(500).json({
      code: 500,
      estado: false,
      msg: error.message
    });
  }
}

module.exports = {
    createUser
};
