const { body } = require('express-validator');

const validationSchema = [
  body("rut")
    .exists({ checkFalsy: true }).withMessage("El Rut es obligatorio").bail()
    .isLength({ min: 7 }).withMessage("Rut de largo mínimo 7 caracteres"),
  body("password")
    .exists({ checkFalsy: true }).withMessage("La contraseña es obligatoria").bail()
    .isLength({ min: 4 }).withMessage("Contrase de largo mínimo 4 caracteres"),
  body("dv")
    .exists({ checkFalsy: true }).withMessage("El dv es obligatorio").bail()
    .isLength({ min: 1 }).withMessage("Dv de largo mínimo 1 caracteres"),
  body('name')
    .exists({ checkFalsy: true }).withMessage("El nombre es obligatorio").bail()
    .isLength({ min: 3 }).withMessage("Debe ser un nombre válido")
];

module.exports = validationSchema;
