const { validationResult, matchedData } = require('express-validator');

const expressValidatorMiddleware = (req, res, next) => {
  
  const errors = validationResult(req);
  if (!errors.isEmpty()) {

    return res.status(400).json({
      code: 400,
      message: 'Error en parametros de entrada',
      typeError: errors.array(),
    });
  }

  req.body = matchedData(req, { locations: ['body'] });
  return next();
};

module.exports = expressValidatorMiddleware;
